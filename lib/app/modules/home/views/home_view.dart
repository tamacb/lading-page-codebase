import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        child: Padding(
          padding: allItemPadding,
          child: Container(
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(flex: 2, child: Image.asset('assets/logo.png')),
                  Expanded(
                    flex: 5,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                'Home',
                                style: GoogleFonts.dmSans(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Container(
                                color: Colors.blue,
                                height: 3.0,
                                width: 40.0,
                              )
                            ],
                          ),
                          height: 40.0,
                        ),
                        Text(
                          'Features',
                          style: GoogleFonts.dmSans(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          'Pricing',
                          style: GoogleFonts.dmSans(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                          'Blog',
                          style: GoogleFonts.dmSans(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10.0, left: 10.0),
                        child: SizedBox(height: 40.0, child: ElevatedButton(onPressed: () {}, child: Text('Get Started'))),
                      ))
                ],
              )),
        ),
        preferredSize: Size.fromHeight(Get.height * 0.2),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: allItemPadding,
          child: Column(
            children: [
              buildHeaderContent(),
              const SizedBox(height: 100.0),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 50.0),
                        child: Image.asset('assets/logo1.png'),
                      )),
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 50.0, right: 50.0),
                        child: Image.asset('assets/logo2.png'),
                      )),
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 50.0, right: 50.0),
                        child: Image.asset('assets/logo3.png'),
                      )),
                  Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 50.0),
                        child: Image.asset('assets/logo4.png'),
                      )),
                ],
              ),
              const SizedBox(height: 100.0),
              buildContentLeftImage(
                  image: 'content1.png',
                  title: 'Subscription index',
                  subTitle:
                      'A daily dataset to keep you up to date on subscription market trends and what\'s happening in your vertical.'),
              const SizedBox(height: 100.0),
              buildContentRightImage(
                  image: 'content2.png',
                  title: 'In-depth metrics',
                  subTitle: 'Accurate, real-time reporting at your fingertips. Getting data has never been easier.'),
              const SizedBox(height: 100.0),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: buildCardItem(
                        image: 'card.png',
                        subTitle: 'See how you stack up against comparable companies in similar stages.',
                        title: 'Benchmarks'),
                  ),
                  Expanded(
                    flex: 1,
                    child: buildCardItem(
                        image: 'calculator.png',
                        subTitle: 'Benchmark the health of your monetization and pricing strategy.',
                        title: 'Pricing Audit'),
                  ),
                  Expanded(
                    flex: 1,
                    child: buildCardItem(
                        image: 'camera.png',
                        subTitle: 'Audit where revenue leakage exists and where you can increase retention.',
                        title: 'Retention Audit'),
                  ),
                ],
              ),
              const SizedBox(height: 100.0),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Get the right plan for future product.',
                        style: GoogleFonts.dmSans(
                          color: Colors.black,
                          fontSize: 60,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Expanded(
                      flex: 1,
                      child: Row(
                        children: [
                          Container(
                            child: const Center(child: Text('Yearly')),
                            height: 45.0,
                            width: 100.0,
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.only(topLeft: Radius.circular(15.0), bottomLeft: Radius.circular(15.0)),
                                color: Colors.deepPurple),
                          ),
                          Container(
                            child: const Center(child: Text('Monthly')),
                            height: 45.0,
                            width: 100.0,
                            decoration: BoxDecoration(
                                borderRadius:
                                    const BorderRadius.only(topRight: Radius.circular(15.0), bottomRight: Radius.circular(15.0)),
                                color: Colors.grey[200]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 100.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Expanded(
                    child: buildExpandedItemPriceList(
                        title: 'Starter',
                        price: 'Free',
                        itemWeb: '1 Website',
                        bandWith: '5 GB Hosting',
                        support: 'Limited Support',
                        getStarted: 'Get Started'),
                  ),
                  Expanded(
                    child: buildExpandedItemPriceList(
                        title: 'Premium',
                        price: '\$29',
                        itemWeb: '10 Website',
                        bandWith: '15 GB Hosting',
                        support: 'Premium Support',
                        getStarted: 'Get Started', colors: Colors.deepOrangeAccent, bottomPadding: 2, topPadding: 2),
                  ),
                  Expanded(
                    child: buildExpandedItemPriceList(
                        title: 'Enterprise',
                        price: '\$49',
                        itemWeb: 'Unlimited Website',
                        bandWith: '50 GB Hosting',
                        support: 'Premium Support',
                        getStarted: 'Get Started'),
                  ),
                ],
              ),
              const SizedBox(height: 100.0),

            ],
          ),
        ),
      ),
    );
  }

  Widget buildExpandedItemPriceList(
      {String? title,
      String? price,
      String? itemWeb,
      String? bandWith,
      String? support,
      String? getStarted,
      double? topPadding,
      double? bottomPadding,
      Color? colors}) {
    return Padding(
      padding: EdgeInsets.only(right: 8.0, left: 8.0, top: topPadding ?? 8, bottom: bottomPadding ?? 8),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15.0), color: colors ?? Colors.grey[50]),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                '$title',
                style: GoogleFonts.dmSans(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('$price', style: GoogleFonts.mulish(
                color: Colors.black,
                fontSize: 60,
                fontWeight: FontWeight.w600,
              ),),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('$itemWeb', style: GoogleFonts.mulish(
                color: Colors.grey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('$bandWith', style: GoogleFonts.mulish(
                color: Colors.grey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text('$support', style: GoogleFonts.mulish(
                color: Colors.grey,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),),
            ),
            SizedBox(height: 50),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: SizedBox(
                width: double.infinity,
                height: 50.0,
                child: ElevatedButton(onPressed: () { },
                child: Text('$getStarted', style: GoogleFonts.dmSans(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),),),
              )
            ),
          ],
        ),
      ),
    );
  }

  Column buildCardItem({String? image, String? title, String? subTitle}) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(height: 80.0, width: 80.0, child: Image.asset('assets/$image')),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            title!,
            style: GoogleFonts.dmSans(
              color: Colors.black,
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            subTitle!,
            style: GoogleFonts.mulish(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      ],
    );
  }

  Row buildHeaderContent() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Text(
                    'Grow your subscription business',
                    style: GoogleFonts.dmSans(
                      color: Colors.black,
                      fontSize: 80,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Wrap(children: [
                    Text(
                      'Outcome-centered products that reduce churn, optimize pricing, and grow your subscription business end-to-end.',
                      style: GoogleFonts.mulish(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    )
                  ]),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: SizedBox(child: Image.asset('assets/skate.png')),
        ),
      ],
    );
  }

  Row buildContentLeftImage({String? title, String? subTitle, String? image}) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: SizedBox(child: Image.asset('assets/$image')),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Text(
                    title!,
                    style: GoogleFonts.dmSans(
                      color: Colors.black,
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Wrap(children: [
                    Text(
                      subTitle!,
                      style: GoogleFonts.mulish(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    )
                  ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Wrap(children: [
                    Text(
                      'Learn more',
                      style: GoogleFonts.mulish(
                        color: Colors.blue,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ]),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Row buildContentRightImage({String? title, String? subTitle, String? image}) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Text(
                    title!,
                    style: GoogleFonts.dmSans(
                      color: Colors.black,
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Wrap(children: [
                    Text(
                      subTitle!,
                      style: GoogleFonts.mulish(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                      ),
                    )
                  ]),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Wrap(children: [
                    Text(
                      'Learn more',
                      style: GoogleFonts.mulish(
                        color: Colors.blue,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  ]),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: SizedBox(child: Image.asset('assets/$image')),
        ),
      ],
    );
  }
}

const allItemPadding = EdgeInsets.only(left: 150.0, top: 8.0, right: 150.0, bottom: 8.0);
